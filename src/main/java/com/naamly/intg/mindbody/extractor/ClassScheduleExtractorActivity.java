package com.naamly.intg.mindbody.extractor;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.db.ClassScheduleRecord;
import com.naamly.intg.mindbody.db.ScheduleDAO;
import com.naamly.intg.mindbody.orchestrator.BaseActivity;
import com.naamly.intg.mindbody.orchestrator.BaseContext;
import com.naamly.intg.mindbody.orchestrator.visits.VisitsContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;

@Component
public class ClassScheduleExtractorActivity extends BaseActivity {

  private static Logger logger = LoggerFactory.getLogger(ClassScheduleExtractorActivity.class);

  private boolean proceedToNextActivity = true;
  @Autowired private ScheduleDAO scheduleDAO;


  @Override
  public BaseContext execute(BaseContext context) {
    try {
      VisitsContext visitsContext = (VisitsContext) context;
      List<JsonObject> classesResponse = visitsContext.getClassesJson();
      if (Objects.isNull(classesResponse)) {
        return visitsContext;
      }
      int count=0;
      for (JsonObject classJson:classesResponse){
        JsonArray classes = (JsonArray) classJson.get("Classes");
        for (int i = 0; i < classes.size(); i++) {
          JsonObject mClass = (JsonObject) classes.get(i);
          ClassScheduleRecord record = new ClassScheduleRecord();
          record.setStudioId(visitsContext.getStudioId());
          record.setClassId(mClass.get("Id").getAsString());
          JsonObject locationJson=(JsonObject)mClass.get("Location");
          record.setLocationId(locationJson.get("Id").getAsString());

          JsonObject classDesc = (JsonObject) mClass.get("ClassDescription");
          String classDescription = classDesc.get("Category").getAsString();
          record.setClassDescription(classDescription);

          JsonObject staff = (JsonObject) mClass.get("Staff");
          String firstName = staff.get("FirstName").getAsString();
          String lastName = staff.get("LastName").getAsString();
          record.setStaffDetails(firstName + "," + lastName);
          String startDate = mClass.get("StartDateTime").getAsString();
          String endDate = mClass.get("EndDateTime").getAsString();
          String startTime =
                  new SimpleDateFormat("HH:mm:ss").format(visitsContext.getFormatter().parse(startDate));
          String enDTime =
                  new SimpleDateFormat("HH:mm:ss").format(visitsContext.getFormatter().parse(endDate));
          if(startDate!=null){
            String actualDate=startDate.split("T")[0];
            record.setScheduledDate(visitsContext.getSampleDateFormatter().parse(actualDate));
          }

          record.setStartTime(startTime);
          record.setEndTime(enDTime);
          record.setCreatedAt(new Timestamp(System.currentTimeMillis()));
          record.setLastUpdatedAt(new Timestamp(System.currentTimeMillis()));
          scheduleDAO.createClassesRecord(record);
          count++;
        }
      }
      logger.debug("Classes count is :: {} ",count);


    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return context;
  }

  @Override
  public boolean proceedToNextActivity() {
    return proceedToNextActivity;
  }
}
