package com.naamly.intg.mindbody.extractor;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.db.ClassVisitRecord;
import com.naamly.intg.mindbody.db.ClientMemberShipRecord;
import com.naamly.intg.mindbody.db.ClientRecord;
import com.naamly.intg.mindbody.db.ScheduleDAO;
import com.naamly.intg.mindbody.outbound.VisitsAtGlanceOutBoundController;
import com.naamly.intg.mindbody.orchestrator.BaseActivity;
import com.naamly.intg.mindbody.orchestrator.BaseContext;
import com.naamly.intg.mindbody.orchestrator.visits.VisitsContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Component
public class ClassVisitExtractorActivity extends BaseActivity {

  private boolean proceedToNextActivity = true;
  @Autowired private ScheduleDAO scheduleDAO;

  @Autowired private VisitsAtGlanceOutBoundController outBoundController;

  private static Logger log = LoggerFactory.getLogger(ClassVisitExtractorActivity.class);

  @Override
  public boolean proceedToNextActivity() {
    return proceedToNextActivity;
  }

  @Override
  public BaseContext execute(BaseContext context) {
    try {
      VisitsContext visitsContext = (VisitsContext) context;
      List<JsonObject> visitsJson = visitsContext.getVisitsJson();
      if (Objects.isNull(visitsJson)) {
        return visitsContext;
      }
      for (int i = 0; i < visitsJson.size(); i++) {
        JsonObject visitJson = visitsJson.get(i);
        JsonObject aClass = (JsonObject) visitJson.get("Class");
        if (Objects.isNull(aClass) || aClass.get("Visits") == null) {
          continue;
        }
        JsonArray clientArray = aClass.get("Visits").getAsJsonArray();
        if (clientArray.size() == 0) {
          continue;
        }
        log.info("clientArray = {} ", clientArray);
        for (int j = 0; j < clientArray.size(); j++) {
          JsonObject client = (JsonObject) clientArray.get(j);
          ClassVisitRecord record = new ClassVisitRecord();
          record.setClassId(aClass.get("Id").getAsString());
          String clientId = client.get("ClientId").getAsString();
          record.setClientId(clientId);
          record.setStudioId(visitsContext.getStudioId());
          JsonObject locationJson = (JsonObject) aClass.get("Location");
          record.setLocationId(locationJson.get("Id").getAsString());
          JsonObject clientJson = outBoundController.getClientInfo(clientId, visitsContext);
          //JsonObject autoPayDetails=outBoundController.getAutoPayDetails(clientId, visitsContext);
          if (clientJson == null) {
            continue;
          }
          JsonArray clients = (JsonArray) clientJson.get("Clients");
          // record.setAppointmentNotes(aClass.get("Notes").getAsString());
          if (clients != null && clients.size() > 0) {
            JsonObject clientInfo = (JsonObject) clients.get(0);
            if (!Objects.isNull(clientInfo)) {
              ClientRecord clientRecord = new ClientRecord();
              ClientMemberShipRecord clientMemberShipRecord=new ClientMemberShipRecord();
              clientRecord.setClientId(clientId);
              clientMemberShipRecord.setClientId(clientId);
              String clientName =
                  clientInfo.get("FirstName").getAsString()
                      + ","
                      + clientInfo.get("LastName").getAsString();
              record.setClientName(clientName);
              clientRecord.setClientName(clientName);
              clientMemberShipRecord.setClientName(clientName);
              if (!clientInfo.get("CreationDate").isJsonNull()) {
                String memberFrom = clientInfo.get("CreationDate").getAsString();
                String actualDate = memberFrom.split("T")[0];
                Timestamp ts =
                        new Timestamp(
                                visitsContext.getSampleDateFormatter().parse(actualDate).getTime());
                clientMemberShipRecord.setMemberFrom(ts);
              }

              if (!clientInfo.get("BirthDate").isJsonNull()) {
                String birthDate = clientInfo.get("BirthDate").getAsString();
                String actualDate = birthDate.split("T")[0];
                Timestamp ts =
                    new Timestamp(
                        visitsContext.getSampleDateFormatter().parse(actualDate).getTime());
                clientRecord.setBirthDate(ts);
                record.setBirthDate(ts);
              }
              if(!Objects.isNull(clientInfo.get("Email").isJsonNull())){
                clientRecord.setEmail(clientInfo.get("Email").getAsString());
              }
              if(!clientInfo.get("MobilePhone").isJsonNull()){
                clientRecord.setPhoneNumber(clientInfo.get("MobilePhone").getAsString());
              }
              clientRecord.setCreatedAt(new Timestamp(System.currentTimeMillis()));
              clientRecord.setLastUpdatedAt(new Timestamp(System.currentTimeMillis()));
              clientMemberShipRecord.setCreatedAt(new Timestamp(System.currentTimeMillis()));
              clientMemberShipRecord.setLastUpdatedAt(new Timestamp(System.currentTimeMillis()));
              scheduleDAO.createClientRecord(clientRecord);
              scheduleDAO.createClientMembershipRecord(clientMemberShipRecord);
              scheduleDAO.createClassVisitRecord(record);
            }

          }
        }
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return context;
  }
}
