package com.naamly.intg.mindbody.config;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileNotFoundException;

@Configuration
public class MindBodyApiConfig {

    public static final Logger logger = LoggerFactory.getLogger(MindBodyApiConfig.class);

    @Bean(name = "mindBodyRestTemplate")
    public RestTemplate prepareRestTemplateForProductService() {
        logger.debug("initialising mindBodyRestTemplate ");
        RequestConfig.Builder requestBuilder = RequestConfig.custom();
        requestBuilder = requestBuilder.setConnectTimeout(10000);

        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        httpClientBuilder.setDefaultRequestConfig(requestBuilder.build());
        CloseableHttpClient httpClient = httpClientBuilder.build();
        HttpComponentsClientHttpRequestFactory rf =
                new HttpComponentsClientHttpRequestFactory(httpClient);
        return new RestTemplate(rf);
    }

    public static File getMBConfig() throws FileNotFoundException {
        logger.debug("MindBodyApiConfig:getContainerRegistriesConfigs   ");
        return ResourceUtils.getFile("classpath:config/MindBody.json");
    }

}
