package com.naamly.intg.mindbody.config;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;

@Component
public class ConfigUtils {

  private static Logger logger = LoggerFactory.getLogger(ConfigUtils.class);
  private Gson gson=new Gson();

  public JsonObject getMBConfiguration() {
    logger.debug("ConfigUtils.getMBConfiguration started Loading configurations ");
    try {
      File mbConfig = MindBodyApiConfig.getMBConfig();
      FileReader reader=new FileReader(mbConfig);
      JsonObject jsonConfig=gson.fromJson(reader,JsonObject.class);
      if (jsonConfig == null || jsonConfig.size() == 0) {
        logger.debug("No configurations found,so exit");
      }
      return jsonConfig;
    } catch (Exception ex) {
      logger.debug("Found Exception while Loading  configurations {} ", ex.getMessage());
    }
    return null;
  }
}
