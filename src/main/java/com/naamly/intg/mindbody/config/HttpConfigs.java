package com.naamly.intg.mindbody.config;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

public class HttpConfigs {

  public static HttpEntity getHttpHeaders(String siteId, String studioName, String apiKey) {
    try {

      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_JSON);
      headers.set("Api-Key", apiKey);
      headers.set("SiteId", siteId);
      headers.set("Name", studioName);
      headers.set("X-Request-Source", "Desktop");
      HttpEntity request = new HttpEntity(headers);
      return request;
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }

  public static String PrepareUrlForClasses(String baseUrl, List<String> classIds) {
    try {
      UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl);
      for (String classId : classIds) {
        uriBuilder.queryParam("classId", classId);
      }
      return uriBuilder.toUriString();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }
}
