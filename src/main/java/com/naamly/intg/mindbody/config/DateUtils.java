package com.naamly.intg.mindbody.config;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {

  public static Date pastDateByDays(Date date, int days) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.add(Calendar.DAY_OF_MONTH, -1*days);
    return cal.getTime();
  }
}
