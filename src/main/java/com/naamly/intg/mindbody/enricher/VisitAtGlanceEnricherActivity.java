package com.naamly.intg.mindbody.enricher;

import com.naamly.intg.mindbody.db.ClassScheduleRecord;
import com.naamly.intg.mindbody.db.ClassVisitRecord;
import com.naamly.intg.mindbody.db.ScheduleDAO;
import com.naamly.intg.mindbody.db.VisitsAtGlanceRecord;
import com.naamly.intg.mindbody.util.MindBodyUtils;
import com.naamly.intg.mindbody.orchestrator.BaseActivity;
import com.naamly.intg.mindbody.orchestrator.BaseContext;
import com.naamly.intg.mindbody.orchestrator.visits.VisitsContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
public class VisitAtGlanceEnricherActivity extends BaseActivity {

  @Autowired private ScheduleDAO scheduleDAO;
  private static Logger log = LoggerFactory.getLogger(VisitAtGlanceEnricherActivity.class);


  @Override
  public BaseContext execute(BaseContext context) {
    try {
      VisitsContext visitsContext = (VisitsContext) context;
      log.info("VisitAtGlanceEnricherActivity activity started ");
      List<ClassScheduleRecord> classScheduleRecordList =
          scheduleDAO.getClassesBySiteId(visitsContext.getStudioId());
      for (ClassScheduleRecord record : classScheduleRecordList) {
        List<ClassVisitRecord> visitRecords = scheduleDAO.getClientsByClassId(record.getClassId());
        List<VisitsAtGlanceRecord> visitsAtGlanceRecords =
            MindBodyUtils.transformVisitsAtGlance(record, visitRecords);
        if (!Objects.isNull(visitsAtGlanceRecords)) {
          for (VisitsAtGlanceRecord visitsAtGlanceRecord : visitsAtGlanceRecords) {
            scheduleDAO.createVisitsAtGlanceRecord(visitsAtGlanceRecord);
          }
        }
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return context;
  }

  @Override
  public boolean proceedToNextActivity() {
    return false;
  }
}
