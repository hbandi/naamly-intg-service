package com.naamly.intg.mindbody;

public interface ScheduleService {

    public void getClasses();
    public void getClassVisits();
}
