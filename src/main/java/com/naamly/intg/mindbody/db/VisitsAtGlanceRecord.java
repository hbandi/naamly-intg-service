package com.naamly.intg.mindbody.db;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class VisitsAtGlanceRecord implements Serializable {

    private String studioId;
    private String locationId;
    private String classId;
    private Date scheduledDate;
    private String startTime;
    private String endTime;
    private String classDescription;
    private String staffDetails;
    private String notes;
    private String isScheduledOnline;
    private String staffAlert;
    private String yellowAlert;
    private String clientId;
    private String clientName;
    private String phone;
    private String status;
    private String memberShipKind;
    private String appointmentNotes;
    private String isUnPaidAppointment;
    private String isFirstVisit;
    private Timestamp birthDate;
    private Timestamp createdAt;
    private Timestamp lastUpdatedAt;


    public String getStudioId() {
        return studioId;
    }

    public void setStudioId(String studioId) {
        this.studioId = studioId;
    }

    @Override
    public String toString() {
        return "ScheduleAtGlanceRecord{" +
                "studioId='" + studioId + '\'' +
                ", locationId='" + locationId + '\'' +
                ", classId='" + classId + '\'' +
                ", scheduledDate=" + scheduledDate +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", classDescription='" + classDescription + '\'' +
                ", staffDetails='" + staffDetails + '\'' +
                ", notes='" + notes + '\'' +
                ", isScheduledOnline='" + isScheduledOnline + '\'' +
                ", staffAlert='" + staffAlert + '\'' +
                ", yellowAlert='" + yellowAlert + '\'' +
                ", clientId='" + clientId + '\'' +
                ", clientName='" + clientName + '\'' +
                ", phone='" + phone + '\'' +
                ", status='" + status + '\'' +
                ", memberShipKind='" + memberShipKind + '\'' +
                ", appointmentNotes='" + appointmentNotes + '\'' +
                ", isUnPaidAppointment='" + isUnPaidAppointment + '\'' +
                ", isFirstVisit='" + isFirstVisit + '\'' +
                ", birthDate=" + birthDate +
                ", createdAt=" + createdAt +
                ", lastUpdatedAt=" + lastUpdatedAt +
                '}';
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public Date getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(Date scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getClassDescription() {
        return classDescription;
    }

    public void setClassDescription(String classDescription) {
        this.classDescription = classDescription;
    }

    public String getStaffDetails() {
        return staffDetails;
    }

    public void setStaffDetails(String staffDetails) {
        this.staffDetails = staffDetails;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getIsScheduledOnline() {
        return isScheduledOnline;
    }

    public void setIsScheduledOnline(String isScheduledOnline) {
        this.isScheduledOnline = isScheduledOnline;
    }

    public String getStaffAlert() {
        return staffAlert;
    }

    public void setStaffAlert(String staffAlert) {
        this.staffAlert = staffAlert;
    }

    public String getYellowAlert() {
        return yellowAlert;
    }

    public void setYellowAlert(String yellowAlert) {
        this.yellowAlert = yellowAlert;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMemberShipKind() {
        return memberShipKind;
    }

    public void setMemberShipKind(String memberShipKind) {
        this.memberShipKind = memberShipKind;
    }

    public String getAppointmentNotes() {
        return appointmentNotes;
    }

    public void setAppointmentNotes(String appointmentNotes) {
        this.appointmentNotes = appointmentNotes;
    }

    public String getIsUnPaidAppointment() {
        return isUnPaidAppointment;
    }

    public void setIsUnPaidAppointment(String isUnPaidAppointment) {
        this.isUnPaidAppointment = isUnPaidAppointment;
    }

    public String getIsFirstVisit() {
        return isFirstVisit;
    }

    public void setIsFirstVisit(String isFirstVisit) {
        this.isFirstVisit = isFirstVisit;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(Timestamp lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }





}
