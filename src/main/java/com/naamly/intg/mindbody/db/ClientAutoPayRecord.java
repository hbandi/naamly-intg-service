package com.naamly.intg.mindbody.db;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

public class ClientAutoPayRecord implements Serializable {

    private String clientId;
    private String clientName;
    private String amount;
    private Timestamp lastAutoPay;
    private Timestamp createdAt;
    private Timestamp lastUpdatedAt;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientAutoPayRecord that = (ClientAutoPayRecord) o;
        return Objects.equals(clientId, that.clientId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId);
    }

    @Override
    public String toString() {
        return "ClientAutoPay{" +
                "clientId='" + clientId + '\'' +
                ", clientName='" + clientName + '\'' +
                ", amount='" + amount + '\'' +
                ", lastAutoPay=" + lastAutoPay +
                ", createdAt=" + createdAt +
                ", lastUpdatedAt=" + lastUpdatedAt +
                '}';
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Timestamp getLastAutoPay() {
        return lastAutoPay;
    }

    public void setLastAutoPay(Timestamp lastAutoPay) {
        this.lastAutoPay = lastAutoPay;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(Timestamp lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }
}
