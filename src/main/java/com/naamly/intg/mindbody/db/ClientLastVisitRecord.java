package com.naamly.intg.mindbody.db;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

public class ClientLastVisitRecord implements Serializable {

    private String clientId;
    private String clientName;
    private String visitLocation;
    private String serviceCategory;
    private String visitType;
    private String pricingOptions;
    private Timestamp expirationDate;
    private String bookingMethod;
    private String referralType;
    private String staff;
    private String repOne;
    private String repTwo;
    private String repThree;
    private String phone;
    private String email;
    private Timestamp lastUpdatedAt;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientLastVisitRecord that = (ClientLastVisitRecord) o;
        return Objects.equals(clientId, that.clientId) &&
                Objects.equals(visitLocation, that.visitLocation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId, visitLocation);
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getVisitLocation() {
        return visitLocation;
    }

    public void setVisitLocation(String visitLocation) {
        this.visitLocation = visitLocation;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    public String getVisitType() {
        return visitType;
    }

    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }

    public String getPricingOptions() {
        return pricingOptions;
    }

    public void setPricingOptions(String pricingOptions) {
        this.pricingOptions = pricingOptions;
    }

    public Timestamp getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Timestamp expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getBookingMethod() {
        return bookingMethod;
    }

    public void setBookingMethod(String bookingMethod) {
        this.bookingMethod = bookingMethod;
    }

    public String getReferralType() {
        return referralType;
    }

    public void setReferralType(String referralType) {
        this.referralType = referralType;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getRepOne() {
        return repOne;
    }

    public void setRepOne(String repOne) {
        this.repOne = repOne;
    }

    public String getRepTwo() {
        return repTwo;
    }

    public void setRepTwo(String repTwo) {
        this.repTwo = repTwo;
    }

    public String getRepThree() {
        return repThree;
    }

    public void setRepThree(String repThree) {
        this.repThree = repThree;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Timestamp getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(Timestamp lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }
}
