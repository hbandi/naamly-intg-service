package com.naamly.intg.mindbody.db;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

public class ClientMemberShipRecord implements Serializable {

    private String clientId;
    private String clientName;
    private String memberShipType;
    private String status;
    private Timestamp memberFrom;
    private Timestamp memberTo;
    private Timestamp lastArrival;
    private Timestamp YtdArrival;
    private Timestamp createdAt;
    private Timestamp lastUpdatedAt;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientMemberShipRecord that = (ClientMemberShipRecord) o;
        return clientId.equals(that.clientId);
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getMemberShipType() {
        return memberShipType;
    }

    public void setMemberShipType(String memberShipType) {
        this.memberShipType = memberShipType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getMemberFrom() {
        return memberFrom;
    }

    public void setMemberFrom(Timestamp memberFrom) {
        this.memberFrom = memberFrom;
    }

    public Timestamp getMemberTo() {
        return memberTo;
    }

    public void setMemberTo(Timestamp memberTo) {
        this.memberTo = memberTo;
    }

    public Timestamp getLastArrival() {
        return lastArrival;
    }

    public void setLastArrival(Timestamp lastArrival) {
        this.lastArrival = lastArrival;
    }

    public Timestamp getYtdArrival() {
        return YtdArrival;
    }

    public void setYtdArrival(Timestamp ytdArrival) {
        YtdArrival = ytdArrival;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(Timestamp lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId);
    }


    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}
