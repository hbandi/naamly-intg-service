package com.naamly.intg.mindbody.db;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class ClassScheduleRecord implements Serializable {

  private String studioId;
  private String locationId;
  private String classId;
  private Date scheduledDate;
  private String startTime;
  private String endTime;
  private String classDescription;
  private String staffDetails;
  private String notes;
  private String isScheduledOnline;
  private String staffAlert;
  private String yellowAlert;
  private Timestamp createdAt;
  private Timestamp lastUpdatedAt;


  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getLastUpdatedAt() {
    return lastUpdatedAt;
  }

  public void setLastUpdatedAt(Timestamp lastUpdatedAt) {
    this.lastUpdatedAt = lastUpdatedAt;
  }


  public String getStudioId() {
    return studioId;
  }

  public void setStudioId(String studioId) {
    this.studioId = studioId;
  }

  public String getLocationId() {
    return locationId;
  }

  public void setLocationId(String locationId) {
    this.locationId = locationId;
  }

  public String getClassId() {
    return classId;
  }

  public void setClassId(String classId) {
    this.classId = classId;
  }

  public Date getScheduledDate() {
    return scheduledDate;
  }

  public void setScheduledDate(Date scheduledDate) {
    this.scheduledDate = scheduledDate;
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }

  public String getClassDescription() {
    return classDescription;
  }

  public void setClassDescription(String classDescription) {
    this.classDescription = classDescription;
  }

  public String getStaffDetails() {
    return staffDetails;
  }

  public void setStaffDetails(String staffDetails) {
    this.staffDetails = staffDetails;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public String getIsScheduledOnline() {
    return isScheduledOnline;
  }

  public void setIsScheduledOnline(String isScheduledOnline) {
    this.isScheduledOnline = isScheduledOnline;
  }

  public String getStaffAlert() {
    return staffAlert;
  }

  public void setStaffAlert(String staffAlert) {
    this.staffAlert = staffAlert;
  }

  public String getYellowAlert() {
    return yellowAlert;
  }

  public void setYellowAlert(String yellowAlert) {
    this.yellowAlert = yellowAlert;
  }
}
