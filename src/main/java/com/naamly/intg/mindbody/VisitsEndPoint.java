package com.naamly.intg.mindbody;

import com.naamly.intg.mindbody.orchestrator.visits.VisitsAtGlanceFlowManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/1.0")
public class VisitsEndPoint {

    @Autowired
    private VisitsAtGlanceFlowManager visitsAtGlanceFlowManager;


    @GetMapping(value = "/visits")
    public @ResponseBody void status(){
        visitsAtGlanceFlowManager.execute();
    }
}
