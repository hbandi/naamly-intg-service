package com.naamly.intg.mindbody.orchestrator.visits;

import com.naamly.intg.mindbody.api.ClassesAPIActivity;
import com.naamly.intg.mindbody.api.VisitsAPIActivity;
import com.naamly.intg.mindbody.enricher.VisitAtGlanceEnricherActivity;
import com.naamly.intg.mindbody.extractor.ClassScheduleExtractorActivity;
import com.naamly.intg.mindbody.extractor.ClassVisitExtractorActivity;
import com.naamly.intg.mindbody.orchestrator.BaseActivity;
import com.naamly.intg.mindbody.orchestrator.BaseOrchestrator;
import com.naamly.intg.mindbody.orchestrator.visits.activity.CleanUpActivity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VisitsAtGlanceOrchestrator extends BaseOrchestrator {

  private static Logger logger = LoggerFactory.getLogger(VisitsAtGlanceOrchestrator.class);

  @Autowired private ClassesAPIActivity classesAPIActivity;
  @Autowired private VisitsAPIActivity visitsAPIActivity;
  @Autowired private ClassScheduleExtractorActivity classScheduleExtractorActivity;
  @Autowired private ClassVisitExtractorActivity classVisitExtractorActivity;
  @Autowired private VisitAtGlanceEnricherActivity visitAtGlanceEnricherActivity;
  @Autowired private CleanUpActivity cleanUpActivity;

  public List<BaseActivity> getActivities() {
    logger.info("activities been called {} ", classesAPIActivity);
    List<BaseActivity> activities = new ArrayList<>();
    activities.add(cleanUpActivity);
    activities.add(classesAPIActivity);
    activities.add(classScheduleExtractorActivity);
    activities.add(visitsAPIActivity);
    activities.add(classVisitExtractorActivity);
    activities.add(visitAtGlanceEnricherActivity);
    return activities;
  }
}
