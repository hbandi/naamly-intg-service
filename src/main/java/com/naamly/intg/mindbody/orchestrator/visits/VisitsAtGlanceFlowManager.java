package com.naamly.intg.mindbody.orchestrator.visits;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.config.ConfigUtils;
import com.naamly.intg.mindbody.orchestrator.BaseContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class VisitsAtGlanceFlowManager {

  private static Logger logger = LoggerFactory.getLogger(ConfigUtils.class);
  @Autowired private ConfigUtils configUtils;

  @Autowired
  private VisitsAtGlanceOrchestrator visitsAtGlanceOrchestrator;

  public BaseContext execute() {
    try {
      JsonObject configData = configUtils.getMBConfiguration();
      logger.info("VisitsAtGlanceFlowManager.execute started with configData {} ", configData);
      JsonArray studios = (JsonArray) configData.get("studios");
      logger.info("VisitsAtGlanceFlowManager.execute studios {} ", studios);

      if (Objects.isNull(studios)) {
        logger.debug("Studios is null,so return here ");
        return null;
      }
      String apiKey = configData.get("apiKey").getAsString();
      String startDate=configData.get("startDate").getAsString();
      String endDate=configData.get("endDate").getAsString();
      for (int i = 0; i < studios.size(); i++) {
        JsonObject studio = (JsonObject) studios.get(i);
        VisitsContext context = new VisitsContext();
        context.setStudioId(studio.get("siteId").getAsString());
        context.setStudioName(studio.get("studioName").getAsString());
        context.setApiKey(apiKey);
        context.setStartDate(startDate);
        context.setEndDate(endDate);
        visitsAtGlanceOrchestrator.start(context);
      }
      return null;
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.error(" Found Exception in CrawlerWorkFlowManager Exception : {} ", ex.getMessage());
    }
    return null;
  }
}
