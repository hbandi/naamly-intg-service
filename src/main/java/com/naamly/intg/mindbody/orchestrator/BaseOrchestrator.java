package com.naamly.intg.mindbody.orchestrator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public abstract class BaseOrchestrator {

  private static Logger log = LoggerFactory.getLogger(BaseOrchestrator.class);

  public BaseOrchestrator() {}

  public void start(BaseContext context) {
    List<BaseActivity> activities = getActivities();
    log.info("activities is {} ",activities);
    for (BaseActivity activity : activities) {
      long startTime = System.currentTimeMillis();
      activity.execute(context);
      log.info(
          "Total time spent in activity "
              + activity
              + " "
              + (System.currentTimeMillis() - startTime));
      if (!activity.proceedToNextActivity()) {
        return;
      }
    }
  }

  public abstract List<BaseActivity> getActivities();
}
