package com.naamly.intg.mindbody.orchestrator.visits.activity;

import com.naamly.intg.mindbody.db.ScheduleDAO;
import com.naamly.intg.mindbody.orchestrator.BaseActivity;
import com.naamly.intg.mindbody.orchestrator.BaseContext;
import com.naamly.intg.mindbody.orchestrator.visits.VisitsContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CleanUpActivity extends BaseActivity {

  private static Logger logger = LoggerFactory.getLogger(CleanUpActivity.class);

  private boolean proceedToNextActivity = true;

  @Override
  public boolean proceedToNextActivity() {
    return proceedToNextActivity;
  }

  @Autowired private ScheduleDAO scheduleDAO;

  @Override
  public BaseContext execute(BaseContext context) {
    try {
      VisitsContext visitsContext = (VisitsContext) context;
      logger.debug("CleanUpActivity.execute has been called ");
      String studioId = visitsContext.getStudioId();
      scheduleDAO.deleteClassesBySiteId(studioId);
      logger.info("StudioId {} classes has been deleted successfully ");
      scheduleDAO.deleteVisitsBySiteId(studioId);
      logger.info("StudioId {} visits has been deleted successfully ");
    } catch (Exception ex) {
      logger.error("Found error while cleanup activity  exception is {} ", ex);
    }
    return null;
  }
}
