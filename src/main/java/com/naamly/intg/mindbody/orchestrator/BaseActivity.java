package com.naamly.intg.mindbody.orchestrator;

public abstract class BaseActivity {

  public abstract boolean proceedToNextActivity();
  public abstract BaseContext execute(BaseContext context);
}
