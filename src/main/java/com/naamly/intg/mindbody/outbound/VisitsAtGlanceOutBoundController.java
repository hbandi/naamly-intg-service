package com.naamly.intg.mindbody.outbound;

import com.google.gson.JsonObject;
import com.naamly.intg.mindbody.config.ConfigUtils;
import com.naamly.intg.mindbody.config.HttpConfigs;
import com.naamly.intg.mindbody.orchestrator.visits.VisitsContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Objects;

@Component
public class VisitsAtGlanceOutBoundController {

  private static Logger log = LoggerFactory.getLogger(VisitsAtGlanceOutBoundController.class);

  @Autowired private ConfigUtils configUtils;

  @Autowired
  @Qualifier("mindBodyRestTemplate")
  private RestTemplate mindBodyRestTemplate;

  public JsonObject getClientInfo(String clientId, VisitsContext visitsContext) {
    try {
      JsonObject config = configUtils.getMBConfiguration();
      String clientUrl = config.get("clientUrl").getAsString();
      UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(clientUrl);
      uriBuilder.queryParam("ClientIds", clientId);
      String apiKey = visitsContext.getApiKey();
      String studioId = visitsContext.getStudioId();
      String studioName = visitsContext.getStudioName();
      HttpEntity request = HttpConfigs.getHttpHeaders(studioId, studioName, apiKey);
      if (Objects.isNull(request)) {
        log.debug("ClassesAPIActivity.getClient prepared request is null,so ignore ");
      }
      log.info(" Class URL= {} ", uriBuilder.toUriString());
      ResponseEntity<String> response =
          mindBodyRestTemplate.exchange(
              uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
      String body = response.getBody();
      if (body == null) {
        return null;
      }
      JsonObject client = visitsContext.getGson().fromJson(body, JsonObject.class);
      return client;
    } catch (Exception ex) {
      log.error(
          "Found error while fetching client info for the client id {} and exception is {} ",
          clientId,
          ex);
    }
    return null;
  }

  public String getClassInfo(
      String startDate,
      String endDate,
      boolean isHideCanceledClasses,
      int limit,
      int offSet,
      VisitsContext visitsContext) {
    try {
      JsonObject config = configUtils.getMBConfiguration();
      String classUrl = config.get("classesUrl").getAsString();
      UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(classUrl);
      uriBuilder
          .queryParam("StartDateTime", startDate)
          .queryParam("EndDateTime", endDate)
          .queryParam("HideCanceledClasses", isHideCanceledClasses)
          .queryParam("Limit", limit)
          .queryParam("OffSet", offSet);
      String apiKey = visitsContext.getApiKey();
      String studioId = visitsContext.getStudioId();
      String studioName = visitsContext.getStudioName();
      HttpEntity request = HttpConfigs.getHttpHeaders(studioId, studioName, apiKey);

      ResponseEntity<String> response =
          mindBodyRestTemplate.exchange(
              uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
      return response.getBody();
    } catch (Exception ex) {
      log.error(
          "Found error while fetching class info for the startDate {},endDate {}  and exception is {} ",
          startDate,
          endDate,
          ex);
    }
    return null;
  }

  public String getVisitsInfo(String classId, VisitsContext visitsContext) {
    try {
      JsonObject config = configUtils.getMBConfiguration();
      String visitorsUrl = config.get("visitorsUrl").getAsString();
      String apiKey = visitsContext.getApiKey();
      String studioId = visitsContext.getStudioId();
      String studioName = visitsContext.getStudioName();
      HttpEntity request = HttpConfigs.getHttpHeaders(studioId, studioName, apiKey);
      UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(visitorsUrl);
      uriBuilder.queryParam("classId", classId);
      ResponseEntity<String> response =
          mindBodyRestTemplate.exchange(
              uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
      return response.getBody();

    } catch (Exception ex) {
      log.error(
          "Found error while fetching visits info for the clientId {} and exception is {} ",
          classId,
          ex);
    }
    return null;
  }
  public JsonObject getActiveClientMemberShipDetails(String clientId, VisitsContext visitsContext) {
    try {
      JsonObject config = configUtils.getMBConfiguration();
      String clientUrl = config.get("activeClientMemberShipUrl").getAsString();
      UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(clientUrl);
      uriBuilder.queryParam("clientId", clientId);
      String apiKey = visitsContext.getApiKey();
      String studioId = visitsContext.getStudioId();
      String studioName = visitsContext.getStudioName();
      HttpEntity request = HttpConfigs.getHttpHeaders(studioId, studioName, apiKey);
      if (Objects.isNull(request)) {
        log.debug("ClassesAPIActivity.getClient prepared request is null,so ignore ");
      }
      log.info(" Class URL= {} ", uriBuilder.toUriString());
      ResponseEntity<String> response =
              mindBodyRestTemplate.exchange(
                      uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
      String body = response.getBody();
      if (body == null) {
        return null;
      }
      JsonObject client = visitsContext.getGson().fromJson(body, JsonObject.class);
      return client;
    } catch (Exception ex) {
      log.error(
              "Found error while fetching client info for the client id {} and exception is {} ",
              clientId,
              ex);
    }
    return null;
  }
  public JsonObject getAutoPayDetails(String clientId, VisitsContext visitsContext){
    try {
      JsonObject config = configUtils.getMBConfiguration();
      String clientUrl = config.get("autoPayUrl").getAsString();
      UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(clientUrl);
      uriBuilder.queryParam("clientId", clientId);
      String apiKey = visitsContext.getApiKey();
      String studioId = visitsContext.getStudioId();
      String studioName = visitsContext.getStudioName();
      HttpEntity request = HttpConfigs.getHttpHeaders(studioId, studioName, apiKey);
      if (Objects.isNull(request)) {
        log.debug("ClassesAPIActivity.getClient prepared request is null,so ignore ");
      }
      log.info(" Class URL= {} ", uriBuilder.toUriString());
      ResponseEntity<String> response =
              mindBodyRestTemplate.exchange(
                      uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
      String body = response.getBody();
      if (body == null) {
        return null;
      }
      JsonObject client = visitsContext.getGson().fromJson(body, JsonObject.class);
      return client;
    } catch (Exception ex) {
      log.error(
              "Found error while fetching client info for the client id {} and exception is {} ",
              clientId,
              ex);
    }
    return null;
  }
}
