package com.naamly.intg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntgApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntgApplication.class, args);
    }

}
