package com.naamly.intg.common.db;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NaamlyDBConfig {

	@Value("${naamly.database.driver}")
	private String databaseDriver;

	private String databaseUrl;

	@Value("${naamly.database.username}")
	private String databaseUser;

	@Value("${naamly.database.password}")
	private String databasePassword;

	@Value("${naamly.database.minimum.connections}")
	private int databaseMinConnections;

	@Value("${naamly.database.maximum.connections}")
	private int databaseMaxConnections;

	@Value("${naamly.database.connection.timeout}")
	private long databaseConnTimeout;

	@Value("${naamly.database.connection.poolname}")
	private String databaseConnPoolname;

	@Value("${naamly.database.liquibase.master}")
	private String changelogMasterPath;

	@Value("${naamly.database.default.schema}")
	private String databaseDefaultSchema;
	
	@Value("${naamly.database.host}")
	private String databaseHost;
	
	@Value("${naamly.database.dbname}")
	private String databaseName;

	public NaamlyDBConfig() {

	}

	public String getDatabaseDriver() {
		return databaseDriver;
	}

	public String getDatabaseUrl() {
		return databaseUrl;
	}

	public String getDatabaseUser() {
		return databaseUser;
	}

	public String getDatabasePassword() {
		return databasePassword;
	}

	public int getDatabaseMinConnections() {
		return databaseMinConnections;
	}

	public int getDatabaseMaxConnections() {
		return databaseMaxConnections;
	}

	public long getDatabaseConnTimeout() {
		return databaseConnTimeout;
	}

	public String getDatabaseConnPoolname() {
		return databaseConnPoolname;
	}

	public String getChangelogMasterPath() {
		return changelogMasterPath;
	}

	public String getDatabaseDefaultSchema() {
		return databaseDefaultSchema;
	}

	public String getDatabaseHost() {
		return databaseHost;
	}

	public void setDatabaseHost(String databaseHost) {
		this.databaseHost = databaseHost;
	}

	public String getDatabaseName() {
		return databaseName;
	}



	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public void setDatabaseDriver(String databaseDriver) {
		this.databaseDriver = databaseDriver;
	}

	public void setDatabaseUrl(String databaseUrl) {
		this.databaseUrl = databaseUrl;
	}

	public void setDatabaseUser(String databaseUser) {
		this.databaseUser = databaseUser;
	}

	public void setDatabasePassword(String databasePassword) {
		this.databasePassword = databasePassword;
	}

	public void setDatabaseMinConnections(int databaseMinConnections) {
		this.databaseMinConnections = databaseMinConnections;
	}

	public void setDatabaseMaxConnections(int databaseMaxConnections) {
		this.databaseMaxConnections = databaseMaxConnections;
	}

	public void setDatabaseConnTimeout(long databaseConnTimeout) {
		this.databaseConnTimeout = databaseConnTimeout;
	}

	public void setDatabaseConnPoolname(String databaseConnPoolname) {
		this.databaseConnPoolname = databaseConnPoolname;
	}

	public void setChangelogMasterPath(String changelogMasterPath) {
		this.changelogMasterPath = changelogMasterPath;
	}

	public void setDatabaseDefaultSchema(String databaseDefaultSchema) {
		this.databaseDefaultSchema = databaseDefaultSchema;
	}

	@Override
	public String toString() {
		return "NaamlyDBConfig [databaseDriver=" + databaseDriver + ", databaseUrl=" + databaseUrl + ", databaseUser="
				+ databaseUser + ", databaseMinConnections="
				+ databaseMinConnections + ", databaseMaxConnections=" + databaseMaxConnections
				+ ", databaseConnTimeout=" + databaseConnTimeout + ", databaseConnPoolname=" + databaseConnPoolname
				+ ", changelogMasterPath=" + changelogMasterPath + ", databaseDefaultSchema=" + databaseDefaultSchema
				+ ", databaseHost=" + databaseHost + ", databaseName=" + databaseName + "]";
	}
	
	
	

}
