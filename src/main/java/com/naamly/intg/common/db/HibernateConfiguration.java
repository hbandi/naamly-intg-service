package com.naamly.intg.common.db;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class HibernateConfiguration {

	@Autowired
	DataSource dataSource;

	@Bean
	public SessionFactory getHibernateSession() {
		org.hibernate.cfg.Configuration configuration = new org.hibernate.cfg.Configuration()
				.configure("hibernate/hibernate.cfg.xml");
		SessionFactory sf = configuration.buildSessionFactory(new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).applySetting(Environment.DATASOURCE, dataSource).build());
		return sf;
	}
}
