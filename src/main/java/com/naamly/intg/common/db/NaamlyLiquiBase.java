package com.naamly.intg.common.db;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;


@Component
public class NaamlyLiquiBase extends SpringLiquibase {
	
	public NaamlyLiquiBase() {
		super();
	}
	
	@PostConstruct
	public void initialize() {
		java.security.Security.setProperty("networkaddress.cache.ttl" , "1");
	    java.security.Security.setProperty("networkaddress.cache.negative.ttl" , "3");
	}
		
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Value("#{naamlyDBConfig.changelogMasterPath}")
	public void setChangeLog(String dataModel) {
		this.changeLog = dataModel;
	}
	
	@Value("#{naamlyDBConfig.databaseDefaultSchema}")
	public void setDefaultSchema(String defaultSchema) {
		this.defaultSchema = defaultSchema;
	}
	


}
