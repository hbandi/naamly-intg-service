package com.naamly.intg.common.db;

import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DBDataSource extends HikariDataSource implements InitializingBean{

	public static final Logger log = LoggerFactory.getLogger(DBDataSource.class);
	
	@Autowired
	NaamlyDBConfig envConfig;


	public DBDataSource() {
		
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		
		log.info("NaamlyDBConfig " + envConfig);
		
		this.setDriverClassName(envConfig.getDatabaseDriver());
		String url = "jdbc:mysql://" + envConfig.getDatabaseHost() + "/" + envConfig.getDatabaseName();
		String password = envConfig.getDatabasePassword();
		
		this.setJdbcUrl(url);
		this.setUsername(envConfig.getDatabaseUser());
		this.setPassword(password);
		this.setMaximumPoolSize(envConfig.getDatabaseMaxConnections());
		this.setMinimumIdle(envConfig.getDatabaseMinConnections());
		this.setConnectionTimeout(envConfig.getDatabaseConnTimeout());
		this.setPoolName(envConfig.getDatabaseConnPoolname());
		
	}

}
